﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Attendee conf;


        public MainWindow()
        {
            conf = new Attendee();

            InitializeComponent();
            RegistrationType_Combo.Items.Add("Choice");
            RegistrationType_Combo.Items.Add("Student");
            RegistrationType_Combo.Items.Add("Oragniser");
            RegistrationType_Combo.Items.Add("Full");
            RegistrationType_Combo.SelectedIndex = 0;
        }

        private void FirstName_Text_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void RegistrationType_Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Clear_Click(object sender, RoutedEventArgs e) //Will set the text boxes that the "Clear" button will be clearing when clicked.
        {
            FirstName_Text.Text = String.Empty;
            SecondName_Text.Text = String.Empty;
            AttendeeRef_Text.Text = String.Empty;
            InstitutionName_Text.Text = String.Empty;
            ConferenceName_Text.Text = String.Empty;
            PaperTitle_Text.Text = String.Empty;

        }

        private void Set_Click(object sender, RoutedEventArgs e)
        {
            conf.firstname = FirstName_Text.Text;
            conf.secondName = SecondName_Text.Text;
            conf.attendRef = AttendeeRef_Text.Text;
            int val = 0;
            bool res = Int32.TryParse(AttendeeRef_Text.Text, out val);
            if (res == true && val > 39999 && val < 60001)
            {
                conf.attendRef = AttendeeRef_Text.Text;
            }
            else
            {
                MessageBox.Show("Error");
                return;
            }
            
            conf.institution = InstitutionName_Text.Text;
            conf.conference = ConferenceName_Text.Text;
            conf.paperTitle = PaperTitle_Text.Text;




        }

        private void Invoice_Click(object sender, RoutedEventArgs e)
        {
            string FirstNameText = conf.firstname;
            string SecondNameText = conf.secondName;
            string GetCostText = conf.getCost;
            Form1 invo = new Form1(FirstNameText, SecondNameText, GetCostText); //Passes the first and second name, as well as the Cost from whichever option the user has chosen into the form.
            invo.ShowDialog();
        }

        private void Certificate_Click(object sender, RoutedEventArgs e)
        {
            string FirstNameText = conf.firstname;
            string SecondNameText = conf.secondName;
            string ConferenceName = conf.conference;

            Form2 cert = new Form2(FirstNameText, SecondNameText, ConferenceName); //This passes the first and second name as well as the Conference Name into the form.
            cert.ShowDialog();

        }

        private void Paid_Check_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Presenter_Check_Checked(object sender, RoutedEventArgs e)
        {
            PaperTitle_Text.IsEnabled = false; //If the Presenter Checkbox is ticked, then the PaperTitle TextBox will not allow users to edit the box.
        }

        private void Get_Click(object sender, RoutedEventArgs e)
        {
            if (RegistrationType_Combo.Text.Contains("Student"))
            {
                string Price1 = "£300"; //If the user selects "student" from the drop down menu, the invoice will show the price as £300.
                conf.getCost = Price1;
            }
            else if (RegistrationType_Combo.Text.Contains("Organiser"))
            {
                string Price2 = "£0"; //If the user selects "Organiser" from the drop down menu, the invoice will show their price as £0.
                conf.getCost = Price2;
            }
            else if (RegistrationType_Combo.Text.Contains("Full"))
            {
                string Price3 = "£500"; //If the user selects "Full" from the drop down menu, the invoice will show their price as £500.
                conf.getCost = Price3;
            }
            else if (RegistrationType_Combo.Text.Contains("Choice"))
            {
                MessageBox.Show("Please select a valid choice from the drop down menu"); //If the user leaves the drop down as "Choice" or selects it, a message box will say that they need to select a different option.
            }
            else
            {
                return;
            }
        }
        

    }
}
