﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cw1_2017
{
    public partial class Form1 : Form
    {
        Attendee conf = new Attendee();
        public Form1(string ForeName, string Surname, string Payment)
        {
            InitializeComponent();
            conf.firstname = ForeName;
            conf.secondName = Surname;
            conf.getCost = Payment;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.textBox1.Text += conf.firstname + Environment.NewLine;
            this.textBox1.Text += conf.secondName + Environment.NewLine;
            this.textBox1.Text += conf.getCost + Environment.NewLine;
        }
    }
}
