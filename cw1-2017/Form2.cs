﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cw1_2017
{
    public partial class Form2 : Form
    {
        Attendee conf = new Attendee();

        public Form2(string ForeName, string Surname, string ConfName)
        {
            InitializeComponent();
            conf.firstname = ForeName;
            conf.secondName = Surname;
            conf.conference = ConfName;

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.textBox2.Text += "This is to certify that " + conf.firstname + " " + conf.secondName + " attended " + conf.conference;
        }
    }
}
