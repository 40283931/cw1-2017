﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    class Attendee
    {
        public string attendRef
        {
            get { return AttendRef;}
            set { AttendRef = value; }
        }
        private string AttendRef;

        public string institution

        { 
            get { return Institution;}
            set  { Institution = value; }
        }
        private string Institution;

        public string conference
        {
            get { return Conference; }
            set { Conference = value;}
        }
        private string Conference;

        public string registration
        {
            get { return Registration; }
            set { Registration = value;}
        }
        private string Registration;

        public bool paid
        {
            get { return Paid;}
            set { Paid = value;}
        }
        private bool Paid;

        public bool presenter
        {
            get { return Presenter;}
            set { Presenter = value;}
        }
        private bool Presenter;

        public string paperTitle
        {
            get { return PaperTitle; }
            set { PaperTitle = value; }
        }
        private  string PaperTitle;

        public string getCost
        {
            get { return GetCost; }
            set { GetCost = value; }
        }
        private  string GetCost;

        public string firstname
        {
            get { return FirstName; }
            set { FirstName = value; }
        }
        private string FirstName;

        public string secondName
        {
            get { return SecondName; }
            set { SecondName = value; }
        }
        private string SecondName;
        

    }
}
